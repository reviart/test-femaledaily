const { pool } = require('../configs/config')

exports.getItems = async (req, res) => {
  let query = ''

  if (req.query.pivot) {
    query = `select firstname, lastname, email,
      sum(case when item='Barang1' then quantity else 0 end) Barang1,
      sum(case when item='Barang2' then quantity else 0 end) Barang2,
      sum(case when item='Barang3' then quantity else 0 end) Barang3,
      sum(case when item='Barang4' then quantity else 0 end) Barang4,
      sum(case when item='Barang5' then quantity else 0 end) Barang5,
      sum(case when item='Barang6' then quantity else 0 end) Barang6,
      sum(case when item='Barang7' then quantity else 0 end) Barang7,
      sum(case when item='Barang8' then quantity else 0 end) Barang8,
      sum(case when item='Barang9' then quantity else 0 end) Barang9,
      sum(case when item='Barang10' then quantity else 0 end) Barang10
      from items group by firstname, lastname, email`
  } else {
    query = 'select * from items'
  }

  const { rows: items, rowCount: total } = await pool.query(query)

  return res.status(200).json({
    message: `Data ${total > 0 ? '' : 'not '}available`,
    status: 200,
    total,
    items
  })
}
