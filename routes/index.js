const { Router } = require('express')
const { getItems } = require('../controllers/items')
const router = Router()

router.get('/', async (request, response) => {
  response.status(200).json({
    message: 'Hello world'
  })
})
router.get('/items', getItems)

module.exports = router
