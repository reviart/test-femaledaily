const express = require('express')
const routes = require('./routes')
const app = express()
const port = process.env.PORT || 3001

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use('/', routes)

const start = () => {
  app.listen(port, () =>
    console.log(`Server running on http://localhost:${port}`)
  )
}

start()

module.exports = app