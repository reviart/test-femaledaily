CREATE TABLE items (
  id SERIAL PRIMARY KEY,
  firstname VARCHAR(255) NOT NULL,
  lastname VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  item VARCHAR(255) NOT NULL,
  quantity INTEGER NOT NULL,
  total_price DECIMAL(18, 2) NOT NULL
);

INSERT INTO items (firstname, lastname, email, item, quantity, total_price)
VALUES  ('Tommy', 'Bejo', 'tommy@mail.com', 'Barang1', 2, 100000);
INSERT INTO items (firstname, lastname, email, item, quantity, total_price)
VALUES  ('Joko', 'Widodo', 'joko@mail.com', 'Barang2', 1, 50000);
INSERT INTO items (firstname, lastname, email, item, quantity, total_price)
VALUES  ('Jusuf', 'Kalla', 'jusuf@mail.com', 'Barang3', 3, 150000);
INSERT INTO items (firstname, lastname, email, item, quantity, total_price)
VALUES  ('Tommy', 'Bejo', 'tommy@mail.com', 'Barang2', 2, 100000);
INSERT INTO items (firstname, lastname, email, item, quantity, total_price)
VALUES  ('Shafira', 'Arifin', 'shafira@mail.com', 'Barang4', 2, 150000);
INSERT INTO items (firstname, lastname, email, item, quantity, total_price)
VALUES  ('Linda', 'Sari', 'linda@mail.com', 'Barang5', 4, 200000);
INSERT INTO items (firstname, lastname, email, item, quantity, total_price)
VALUES  ('Gofin', 'Finda', 'gofin@mail.com', 'Barang6', 1, 30000);
INSERT INTO items (firstname, lastname, email, item, quantity, total_price)
VALUES  ('Lutfi', 'Hakim', 'lutfi@mail.com', 'Barang7', 5, 500000);
INSERT INTO items (firstname, lastname, email, item, quantity, total_price)
VALUES  ('Shafira', 'Arifin', 'shafira@mail.com', 'Barang9', 2, 150000);
INSERT INTO items (firstname, lastname, email, item, quantity, total_price)
VALUES  ('Robert', 'Garcia', 'robert@mail.com', 'Barang10', 3, 150000);